import { useSelector } from "react-redux";
import { Redirect, Route as ReactDomRoute } from "react-router-dom";

const Route = ({ isPrivate = false, component: Component, ...rest }) => {
  const { isLogged } = useSelector((state) => state);

  console.log("islogged: ", isLogged);
  return (
    //isPrivate  isLogged    redirect
    //false         false   /component
    //false         true    /dashboard
    //true          false   /login
    //true          true    /dashboard

    <ReactDomRoute
      {...rest}
      render={() => {
        return isPrivate === !!isLogged ? (
          <Component />
        ) : (
          <Redirect to={isPrivate ? "/login" : "/dashboard"} />
        );
      }}
    />
  );
};
export default Route;

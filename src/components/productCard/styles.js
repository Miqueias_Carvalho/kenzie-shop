import styled from "styled-components";

export const Container = styled.div`
  width: 170px;
  height: 320px;
  background-color: var(--gainsboro);
  border-radius: 5px;
  margin: 5px;

  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-end;

  button {
    width: 150px;
    height: 48px;
    margin-top: 16px;
    margin-bottom: 16px;
  }
`;

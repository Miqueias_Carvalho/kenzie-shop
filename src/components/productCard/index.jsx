import { Container } from "./styles";
import { useDispatch } from "react-redux";
import Card from "../card";
import {
  addToCartThunk,
  removeFromCartThunk,
} from "../../store/modules/cart/thunks";
import { useState } from "react";

function ProductCard({ product, isCart = false }) {
  const [id, setId] = useState(1);

  const dispatch = useDispatch();

  //funções
  //-----------------------------------------------------------
  const handleClickAddCart = () => {
    product = { ...product, id };

    setId(id + 1);
    dispatch(addToCartThunk(product));
  };

  const handleClickRemoveFromCart = () => {
    dispatch(removeFromCartThunk(product));
  };

  //return do JSX
  //=============================================================
  return (
    <Container>
      <Card product={product} />

      {isCart ? (
        <button onClick={handleClickRemoveFromCart}>Remover</button>
      ) : (
        <button onClick={handleClickAddCart}>Adicionar ao Carrinho</button>
      )}
    </Container>
  );
}
export default ProductCard;

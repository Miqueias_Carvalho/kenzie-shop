import styled from "styled-components";

export const Image = styled.div`
  width: 150px;
  min-height: 100px;
  margin: 12px 0;

  img {
    width: 100%;
  }
`;

import { Image } from "./sytles";

function Card({ product }) {
  return (
    <>
      <Image>
        <img src={product.image} alt={product.image} />
      </Image>
      <p>{product.name}</p>
      <p>
        {product.price.toLocaleString("pt-BR", {
          style: "currency",
          currency: "BRL",
          minimumFractionDigits: 2,
          maximumFractionDigits: 2,
        })}
      </p>
    </>
  );
}
export default Card;

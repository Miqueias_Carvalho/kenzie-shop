import { Container, MenuContainer, NumItens, CartDetails } from "./styles";
import { useHistory } from "react-router-dom";
import { GiShoppingCart } from "react-icons/gi";
import { FiLogIn, FiLogOut } from "react-icons/fi";
import { useDispatch, useSelector } from "react-redux";
import { logInOutThunk } from "../../store/modules/logged/thunks";
import { clear } from "../../store/modules/cart/actions";

function Toolbar() {
  const { isLogged, cart } = useSelector((state) => state);
  const dispatch = useDispatch();

  const history = useHistory();

  //Funções
  //---------------------------------------------------------------------------
  const handleClickMenu = () => {
    if (isLogged) {
      dispatch(logInOutThunk());
      dispatch(clear());

      history.push("/");
    } else {
      history.push("/login");
    }
  };

  const handleClickHome = () => {
    history.push("/");
  };

  const handleClickCart = () => {
    history.push("/cart");
  };

  //return do JSX
  //============================================================================
  return (
    <Container>
      <div>
        <h2 onClick={handleClickHome}>Kenzie Shop</h2>
        <MenuContainer>
          <div onClick={handleClickCart}>
            <CartDetails>
              <GiShoppingCart />
              <span>Carrinho</span>
              {cart.length > 0 && (
                <NumItens>
                  <p>{cart.length}</p>
                </NumItens>
              )}
            </CartDetails>
          </div>
          <div onClick={handleClickMenu}>
            {isLogged ? (
              <>
                <FiLogOut />
                <span>Sair</span>
              </>
            ) : (
              <>
                <FiLogIn />
                <span>Entrar</span>
              </>
            )}
          </div>
        </MenuContainer>
      </div>
    </Container>
  );
}
export default Toolbar;

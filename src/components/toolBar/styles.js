import styled from "styled-components";

export const Container = styled.nav`
  width: 100vw;
  height: 70px;

  border-bottom: 4px solid var(--gainsboro);
  display: flex;
  align-items: center;
  justify-content: center;

  > div {
    display: flex;
    align-items: center;
    justify-content: space-between;
    width: 90%;

    h2 {
      color: var(--dark-purple);
      cursor: pointer;
    }
  }
`;

export const MenuContainer = styled.div`
  width: 160px;
  display: flex;
  justify-content: space-between;

  > div {
    cursor: pointer;
    color: var(--dark-purple);
  }

  svg {
    font-size: 18px;
    transform: translateY(3px);
    margin-right: 5px;
    color: var(--dark-purple);
  }
`;

export const CartDetails = styled.div`
  position: relative;
`;

export const NumItens = styled.div`
  width: 12px;
  height: 12px;
  text-align: center;
  border-radius: 50%;

  position: absolute;
  top: 0px;
  left: 12px;

  background-color: var(--celurian-frost);
  display: flex;
  justify-content: center;
  align-items: center;

  p {
    color: var(--white);
    font-size: 8px;
    font-weight: 700;
  }
`;

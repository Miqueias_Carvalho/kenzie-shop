import styled from "styled-components";

export const Container = styled.main`
  min-height: 500px;
  margin-top: 48px;
  width: 97%;

  display: flex;
  flex-direction: row-reverse;
  flex-wrap: wrap;

  align-items: flex-start;
  justify-content: center;

  > div {
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    width: 100%;
    max-width: 650px;
  }

  @media (min-width: 380px) {
    flex-wrap: nowrap;
    flex-direction: row;
    justify-content: space-between;
  }
`;

export const ProductContainer = styled.div`
  margin-top: 16px;
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  align-items: center;
`;

export const BoxCart = styled.aside`
  background-color: var(--gainsboro);
  width: 250px;
  border-radius: 5px;

  display: flex;
  justify-content: center;
  align-items: center;

  > div {
    width: 80%;
    margin-top: 8px;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: space-around;

    button {
      width: 100px;
      margin: 16px 0;
    }

    p {
      display: flex;
      justify-content: space-between;
      width: 100%;
      margin: 16px 0;
    }
  }
`;

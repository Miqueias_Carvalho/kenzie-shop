import ProductCard from "../../components/productCard";
import { BoxCart, Container, ProductContainer } from "./styles";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";

function Cart() {
  const { cart, isLogged } = useSelector((state) => state);
  const history = useHistory();

  const handleConfirm = () => {
    if (isLogged) {
      history.push("/dashboard");
    } else {
      history.push("/login");
    }
  };
  //return do JSX
  //=========================================================================
  return (
    <Container>
      <div>
        <h1> Carrinho </h1>
        <ProductContainer>
          {cart.map((product, index) => (
            <ProductCard key={index} product={product} isCart />
          ))}
        </ProductContainer>
      </div>

      <BoxCart>
        <div>
          <p>
            Quantidade de itens: <span>{cart.length}</span>
          </p>
          <p>
            Total:{" "}
            <span>
              {cart
                .reduce((acum, vAtual) => acum + vAtual.price, 0)
                .toLocaleString("pt-BR", {
                  style: "currency",
                  currency: "BRL",
                  minimumFractionDigits: 2,
                  maximumFractionDigits: 2,
                })}
            </span>
          </p>
          <button onClick={handleConfirm}>Confirmar</button>
        </div>
      </BoxCart>
    </Container>
  );
}
export default Cart;

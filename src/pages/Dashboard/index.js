import { Container } from "./styles";
import { useSelector } from "react-redux";
import Card from "../../components/card";
import { Redirect } from "react-router-dom";

function Dashboard() {
  const { cart, isLogged } = useSelector((state) => state);

  //return do JSX
  //==============================================================================
  if (!isLogged) {
    return <Redirect to="/login" />;
  }

  return (
    <Container>
      <h2>Dashboard</h2>

      {cart.map((item, index) => (
        <div key={index}>
          <Card product={item} />
        </div>
      ))}
    </Container>
  );
}
export default Dashboard;

import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;

  > div {
    background-color: var(--gainsboro);
    margin: 8px 0;
    width: 95%;

    display: flex;
    justify-content: space-around;
    align-items: center;
  }
`;

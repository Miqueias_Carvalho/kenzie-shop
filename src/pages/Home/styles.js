import styled from "styled-components";

export const Container = styled.main`
  margin-top: 48px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  > div {
    margin-top: 16px;
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
  }
  /* @media (min-width: 380px) {
  } */
`;

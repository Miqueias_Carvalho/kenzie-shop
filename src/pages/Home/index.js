import ProductCard from "../../components/productCard";
import { Container } from "./styles";
import { useSelector } from "react-redux";

function Home() {
  const { products } = useSelector((state) => state);

  return (
    <Container>
      <h1>home</h1>
      <div>
        {products.map((product, index) => (
          <ProductCard key={index} product={product} />
        ))}
      </div>
    </Container>
  );
}
export default Home;

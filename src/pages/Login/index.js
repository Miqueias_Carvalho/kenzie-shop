import { Container } from "./styles";
import { useDispatch } from "react-redux";
import { useHistory, Redirect } from "react-router-dom";
import { logInOutThunk } from "../../store/modules/logged/thunks";
import * as yup from "yup";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { useSelector } from "react-redux";

function Login() {
  const { isLogged } = useSelector((state) => state);
  const dispatch = useDispatch();
  const history = useHistory();

  //validações
  //----------------------------------------------------------------------------
  const schema = yup.object().shape({
    user: yup.string().required("*Campo Obrigatório"),
    password: yup
      .string()
      .required("*Campo Obrigatório")
      .min(6, "*Mínimo 6 dígitos"),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({ resolver: yupResolver(schema) });

  const onSubmit = (data) => {
    console.log("passou aqui", data);
    dispatch(logInOutThunk());
    history.push("/dashboard");
  };

  //return do JSX
  //==============================================================================
  if (isLogged) {
    return <Redirect to="/" />;
  }

  return (
    <Container>
      <form onSubmit={handleSubmit(onSubmit)}>
        <h2>Login</h2>
        <div>
          <input type="text" placeholder="usuário" {...register("user")} />
          {errors.user && <p>{errors.user.message}</p>}
        </div>
        <div>
          <input
            type="password"
            placeholder="senha"
            {...register("password")}
          />
          {errors.password && <p>{errors.password.message}</p>}
        </div>
        <button type="submit">Entrar</button>
      </form>
    </Container>
  );
}
export default Login;

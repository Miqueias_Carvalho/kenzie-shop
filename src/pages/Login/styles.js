import styled from "styled-components";

export const Container = styled.div`
  margin-top: 48px;
  display: flex;
  justify-content: center;
  align-items: center;

  form {
    width: 100%;
    max-width: 300px;

    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;

    div {
      margin: 8px 0;
      width: 80%;
      min-width: 180px;
      height: 55px;

      input {
        width: 100%;
        height: 40px;
        padding-left: 5px;
        border-radius: 5px;
        border: 1px solid var(--roman-silver);
      }

      p {
        color: red;
        font-size: 12px;
      }
    }

    button {
      width: 100px;
      margin: 16px 0;
    }
  }
`;

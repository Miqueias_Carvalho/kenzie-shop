import { NEWPRODUCT, REMOVEPRODUCT } from "./actionTypes";

//recebe um novo produto
export const addProduct = (product) => ({
  type: NEWPRODUCT.anchor,
  product,
});

//recebe o array filtrado de produtos
export const removeProduct = (products) => ({
  type: REMOVEPRODUCT,
  products,
});

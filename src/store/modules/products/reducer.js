import { NEWPRODUCT, REMOVEPRODUCT } from "./actionTypes";
import Imagem1 from "../../../assets/EchoDot.png";
import Imagem2 from "../../../assets/Kindle.png";
import Imagem3 from "../../../assets/mouse.png";
import Imagem4 from "../../../assets/smartphone2.png";

const products = [
  { name: "Echo Dot", price: 279.0, image: Imagem1 },
  { name: "Kindle Paperwhite", price: 499.0, image: Imagem2 },
  { name: "Mouse gamer", price: 222.0, image: Imagem3 },
  { name: "Smartphone", price: 599.0, image: Imagem4 },
];

const reducerProducts = (state = products, action) => {
  switch (action.type) {
    case NEWPRODUCT:
      return [...state, action.product];
    case REMOVEPRODUCT:
      return action.products;
    default:
      return state;
  }
};

export default reducerProducts;

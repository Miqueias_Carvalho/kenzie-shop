import { ADDToCart, REMOVEFromCart, CLEAR } from "./actionTypes";

const initialState = JSON.parse(localStorage.getItem("@cart")) || [];

const reducerCart = (state = initialState, action) => {
  switch (action.type) {
    case ADDToCart:
      return action.products;
    case REMOVEFromCart:
      return action.products;
    case CLEAR:
      return [];
    default:
      return state;
  }
};

export default reducerCart;

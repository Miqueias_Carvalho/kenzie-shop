import { ADDToCart, CLEAR, REMOVEFromCart } from "./actionTypes";

export const addToCart = (products) => ({
  type: ADDToCart,
  products,
});
export const removeFromCart = (products) => ({
  type: REMOVEFromCart,
  products,
});
export const clear = () => ({
  type: CLEAR,
});

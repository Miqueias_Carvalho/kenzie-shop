import { addToCart, removeFromCart } from "./actions";

export const addToCartThunk = (product) => (dispatch, getState) => {
  const { cart } = getState();

  const list = [...cart, product];
  localStorage.setItem("@cart", JSON.stringify(list));

  dispatch(addToCart(list));
};

export const removeFromCartThunk = (product) => (dispatch, getState) => {
  const { cart } = getState();
  const list = cart.filter(
    (item) => !(item.name === product.name && item.id === product.id)
  );

  localStorage.setItem("@cart", JSON.stringify(list));

  dispatch(removeFromCart(list));
};

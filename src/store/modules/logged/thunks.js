import { logIn_Out } from "./actions";

export const logInOutThunk = () => (dispatch, getState) => {
  //pegando o isLogged que inicia como false -> usuário deslogado
  const { isLogged } = getState();

  //se o usuario entrar, guardar o estado no localstorage, caso deslogue, limpar o local storage
  if (!isLogged) {
    localStorage.setItem("@logado", !isLogged);
  } else {
    localStorage.clear();
  }
  dispatch(logIn_Out(!isLogged));
};

import { iSLOGGED } from "./actionTypes";

export const logIn_Out = (islogged) => ({
  type: iSLOGGED,
  islogged,
});

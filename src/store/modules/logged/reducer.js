import { iSLOGGED } from "./actionTypes";

const logDefault = localStorage.getItem("@logado") || "";

const reducerIsLogged = (state = logDefault, action) => {
  switch (action.type) {
    case iSLOGGED:
      return action.islogged;

    default:
      return state;
  }
};

export default reducerIsLogged;

import Toolbar from "./components/toolBar";
import GlobalStyles from "./styles/global";
import Routes from "./routes";

function App() {
  return (
    <>
      <GlobalStyles />
      <Toolbar />
      <Routes />
    </>
  );
}

export default App;

import { createGlobalStyle } from "styled-components";

export default createGlobalStyle`

* {
    padding: 0;
    margin: 0;
    box-sizing: border-box;
    outline: none;
    }

:root{
    --dark-purple: #251D2C;
    --roman-silver: #7E7C87;
    --gainsboro: #e0dada;
    --celurian-frost: #829BBA;
    --spanish-gray: #9E938D;
    --white: #f5f5f5

}

body{
    background-color: #e0dada42;
    color: var(---dark-purple);
}

a {
     text-decoration: none;

    }

button { 
     cursor: pointer;
     background-color: var(--celurian-frost);
     color: var(--white);
     border-radius: 5px;
     border: none;
     height: 40px;
     font-size: 16px;
        
    }
`;
